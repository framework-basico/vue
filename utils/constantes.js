const constantesBase = {
    ENUMS: {
        TIPO_ALERTA: [
            {chave: 'ERRO', valor: 'Erro'},
            {chave: 'AVISO', valor: 'Aviso'},
            {chave: 'SUCESSO', valor: 'Sucesso'}
        ]
    }
};