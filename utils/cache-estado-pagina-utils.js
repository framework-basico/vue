var CacheEstadoAtual = {};

const PREFIXO_NOME_PAGINA = "estado_atual_pagina_";

CacheEstadoAtual.getEstadoAtualPagina = function (nomePagina) {
    let estadoAtual = localStorage.getItem("estado_atual_pagina_" + nomePagina);

    return estadoAtual ? JSON.parse(estadoAtual) : estadoAtual;
};

CacheEstadoAtual.salvarEstadoAtualPagina = function (nomePagina, estadoAtual) {
    localStorage.setItem(PREFIXO_NOME_PAGINA + nomePagina, JSON.stringify(estadoAtual));
};