function getErroFormatado(error) {
    if (error.response) {
        return limparStringDaLista(error.response.data.message);
    }
    return null;
}

function limparStringDaLista(texto) {
    return texto ? texto.replaceAll('[', '')
        .replaceAll(']', '')
        .replaceAll('\"', "") : texto;
}

function criarAlertaOptions(tipo) {
    return {tipo: tipo ? tipo : 'ERRO', mensagemAlerta: null};
}