function buscarListagem(url, page, size, sort, direction, filter) {

    if (page !== undefined && size !== undefined) {
        url += '?page=' + page + '&size=' + size;
    } else if (page !== undefined) {
        url += '?page=' + page;
    } else if (size !== undefined) {
        url += '?size=' + size;
    }

    if (sort) {
        url += '&sort=' + sort;
    }
    if (direction) {
        url += '&direction=' + direction;
    }
    return axios.get(url);
}

async function buscar(url) {
    return await axios.get(url);
}

async function importar(url) {
    let resposta = await buscar(url);
    return resposta.data;
}

async function buscarRegistro(url, id) {
    return await buscar(url + '/' + id);
}

function salvarRegistro(url, registro, atributoId) {
    if (registro.id || (atributoId && registro[atributoId])) {
        return axios.put(url + '/' + (atributoId ? registro[atributoId] : registro.id), registro);
    }
    return axios.post(url, registro);
}

function enviarArquivo(url, arquivo) {
    var formdata = new FormData();
    formdata.append("arquivo", arquivo, "InstanciaCST.xlsx");

    return axios.post(url, formdata);
}

function deletarRegistro(url, id) {
    return axios.delete(url + '/' + id);
}

function filtroEnumValor(enumObjeto, enums) {
    let enumsFiltradas = filtroEnum(enums, enumObjeto);
    return enumObjeto ? enumsFiltradas[0].valor : '';
}

function filtroEnum(enums, enumObjeto) {
    return enums.filter(semestre => semestre.chave === enumObjeto);
}

function getFilters() {
    let filters = location.search.replace('?', '').split('&');
    let mapFilters = {};
    filters.forEach(filter => {
        mapFilters[filter.split('=')[0]] = filter.split('=')[1];
    });
    return mapFilters;
}