Vue.component('botao-voltar', {
    props: ['funcao'],
    template: `
    <div class="col-12 px-0 my-2">
        <a @click="funcao()" class="btn-voltar">
            <i class="material-icons">keyboard_backspace</i>Voltar
        </a>
    </div>`,
});

Vue.component('botao-cancelar', {
    props: ['funcao'],
    template: `
    <button type="button" class="btn btn-light botao-cancelar float-right mt-5"
     @click="funcao()">
        Cancelar
    </button>`,
});

Vue.component('botao-salvar', {
    props: ['funcao', 'titulo'],
    template: `
    <button type="button" class="btn btn-success botao-principal float-right mt-5"
     @click="funcao">
      {{titulo ? titulo : 'Salvar'}}
    </button>`,
});

Vue.component('botao-selecionar-arquivo', {
    props: ['funcao', 'label', 'textoBotao', 'placeholder'],
    data() {
        return {
            arquivos: [],
        }
    },
    template: `
    <div class="botao-selecionar-arquivo">
        <label class="form-label">{{label}}</label>
        <label :for="'formFile' + _uid" class="btn btn-primary"> {{textoBotao}}
            <input v-on:change="onSelecionarArquivo" ref="meusArquivos" 
            type="file" :id="'formFile' + _uid" multiple>
        </label>
        <span v-show="arquivosSelecionados" class="label">{{arquivosSelecionados}}</span>
        <span v-show="!arquivosSelecionados" class="placeholder">{{placeholder}}</span>
    </div>`,
    computed: {
        arquivosSelecionados() {
            let nomes = [];

            for (let i = 0; i < this.arquivos.length; i++) {
                nomes.push(this.arquivos[i].name);
            }

            return nomes.toString();
        }
    },
    methods: {
        onSelecionarArquivo() {
            this.arquivos = this.$refs.meusArquivos.files;

            this.funcao(this.arquivos.length > 0 ? this.arquivos[0] : undefined);
        }
    }
});

Vue.component('botao-radio', {
    props: ['label', 'valor', 'campo'],
    template: `
    <div class="botao-radio">
        <label v-show="label" class="label">{{label}}</label>
        
        <div class="radio-grupo">        
            <input v-model="valor[campo]" type="radio" value="true"/>
            <label class="botao-radio-texto">Sim</label>
            
            <input v-model="valor[campo]" type="radio" value="false"/>
            <label class="botao-radio-texto">Não</label>        
        </div>
    </div>`
});